# Find duplicates

### Problem
We have a list of **N** files (10.000+). All these files have **same names** and the **same size**, but could have **different content**. Find all the duplicates.

### What we'll be evaluating
We want to understand your problem solving skills and how you approach the problem.

###  Delivery
Once you're done send us your solutions via email or pull request.

###  Talk with us
We're more than happy to give you support and discuss any problem you have along the way. If you have questions don't hesitate in approaching us!